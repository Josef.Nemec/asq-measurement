#!/bin/bash

echo -e "Starting...\n"

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT # Kill background processes on termination
set -e

# Build custom images in minikube environment
minikube image build -t quality_app:1.0 test-infra/quality-app/
echo ""
minikube image build -t server_log_generator:1.0 test-infra/server-log-generator/
echo ""

# Kube Test Infra
cd test-infra/kube-test-infra/
./add_helm_repos.sh
echo ""
./build_infra.sh --values-override ../../_values_override &
cd ../../

sleep 120s
echo ""

# SLO Exporter
cd slo-exporter/
./deploy.sh
cd ../

# HTTP Bot
cd test-infra/http-bot/
./http_bot.py > /dev/null &
cd ../../
echo ""

read -p $'ASQ-measurement successfully started :)\nPress any key to terminate\n' </dev/tty
