# SLO Exporter Tests

Testing code of [seznam/slo-exporter](https://github.com/seznam/slo-exporter) deployment in the `kube-test-infra`.

There are currently 3 tests:
1. SLO-Exporter as a stand-alone Pod, querying Traefik service metrics from Prometheus.
2. SLO-Exporter inside of a Pod with server-log-generator, parsing its log file.
3. SLO-Exporter as a stand-alone Pod (the same as `1.`), querying Prometheus metrics of custom quality service.

## Usage (test build)

1. Deploy: `./deploy.sh`
2. SLO events are in Prometheus. SLOs would now be calculated in Grafana as `successful_events / all_events`. Some Grafana dashboard proposals are at [seznam/slo-exporter](https://github.com/seznam/slo-exporter/tree/master/grafana_dashboards).
3. Undeploy: `./undeploy.sh`

## Notes

This testing folder also contains unused chart `helm-charts/slo-exporter-prometheus_module`, which was later recreated into a more universal modular chart [slo-exporter-modules](helm-charts/slo-exporter-modules).
