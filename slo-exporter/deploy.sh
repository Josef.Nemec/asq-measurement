#!/bin/bash

helm install slo-exporter ./helm-charts/slo-exporter
echo ''
helm install server-log-generator ./helm-charts/server-log-generator
echo ''
helm install quality-app ./helm-charts/quality-app
