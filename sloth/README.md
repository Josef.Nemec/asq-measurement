# Sloth Tests

Testing code for [slok/sloth](https://github.com/slok/sloth) deployment in the [kube-test-infra](../test-infra/kube-test-infra/).

## Usage (test build)

0. Add helm repo: `helm repo add sloth https://slok.github.io/sloth && helm repo update`
1. Deploy: `./deploy.sh`
2. SLOs are in the Prometheus, created by Prometheus Operator; **NOT** in the kube-test-infra Prometheus -> **See `Notes` below**
3. Undeploy: `./undeploy.sh`

### Notes

Prometheus Operator's Prometheus can be accessed by port-forwarding: `kubectl port-forward service/kube-prometheus-stack-prometheus 9099:9090`, in this case at: [http://localhost:9099](http://localhost:9099)

To use it with Grafana, data source needs to be added. In the kube-test-infra, this is done by adding the below into `values_override/grafana.yaml`, under the `datasources` key. Another way would be adding it manually on the Grafana frontend.

```yaml
- name: Prometheus-SLO
  type: prometheus
  url: http://kube-prometheus-stack-prometheus:9090
  access: proxy
  isDefault: false
```
