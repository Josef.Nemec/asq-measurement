#!/bin/bash

helm uninstall kube-prometheus-stack
helm uninstall sloth

kubectl delete PodMonitor traefik
kubectl delete PrometheusServiceLevel traefik-requests
