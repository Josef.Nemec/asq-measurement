#!/bin/bash

WAIT_TIME=30s

helm install -f values_override/kube-prometheus-stack.yaml kube-prometheus-stack prometheus-community/kube-prometheus-stack
helm install -f values_override/sloth.yaml sloth sloth/sloth

echo -e "\nWaiting $WAIT_TIME for sloth to start (slos config needs it ready)..."
sleep $WAIT_TIME

kubectl apply -f pod_monitors/traefik.yaml
kubectl apply -f slos/traefik_plugin.yaml
