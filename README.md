# ASQ Measurement

Repository containing source code of `Accessibility Speed and Quality Measurement` project development. Project is intended to provide Service Level Objective monitoring for Kubernetes based IaaS OpenStack cloud provider.

Despite first testing Sloth, I have reached the conclusion that the configuration possibilities make SLO Exporter a better choice and cover this SLO tool more.
Verbosity on this decision and implementation guide are in the local [Documentation](docs).

## Structure

* [Documentation](docs): Comments on results of the tests and how to use the useful parts of this project
* [Helm Charts](helm-charts): Custom Helm charts, created to solve the objective of this project
* [Flux](flux): Proposed deployment of the charts via Flux

### Testing infrastructure

* [Kube Test Infra](test-infra/kube-test-infra): Testing infrastructure, could possibly find usage outside of this project
* [HTTP Bot](test-infra/http-bot): Simple Python GET request bot, used with `Kube Test Infra`
* [Server Log Generator](test-infra/server-log-generator): Simple Python log generator, outputting server Common Log Format, used in `SLO Exporter` testing
* [Quality App](test-infra/quality-app): Simple Python program, generating Prometheus metrics about quality, used in `SLO Exporter` testing

### SLO Tools Tests

* [SLO Exporter](slo-exporter): Contains testing code for [seznam/SLO Exporter](https://github.com/seznam/slo-exporter)
* [Sloth](sloth): Contains testing code for [slok/Sloth](https://github.com/slok/sloth)

## Startup

### Requirements

- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- [Helm](https://helm.sh/docs/intro/install/)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/):

```bash
minikube start
```

### Usage

1. Run this project:
```bash
./start_testing.sh
```

2. Keep the script running. There is a background port-forwarding and traffic generation.
3. Visit [Prometheus](http://localhost:9090) or [Grafana](http://localhost:3000).
4. Import dashboards of interest in [Grafana GUI](http://localhost:3000/dashboards) to view the counted SLOs.

*Grafana username is `admin` and password is in the script output or gained by running*:

```bash
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

### Cleanup

1. Terminate the script by pressing any key.

2. Delete minikube:
```bash
minikube delete
```
