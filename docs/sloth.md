# Sloth

SLO tool called Sloth gets data by querying Prometheus. It is therefore not part of the tool to interact with the services or deal with their monitoring. Since the usage of Prometheus Operator is expected by Sloth, data would be collected by implementing Kubernetes CRDs (custom resource definitions) called Pod monitors. Data can also be collected by dynamic or static target scraping, but this concerns the configuration of Prometheus Operator and not Sloth itself, so there will be no more written about it.

The tests were done with a simple configuration and also with the Traefik v2 module, which makes it easier to monitor service availability and latency behind Traefik proxy. The intention of the testing phase was to implement solution, that would allow future operators to easily access set SLOs and redefine them without needing to bother with any other configuration. This is done via a newly created helm chart.

Since SLO Exporter is preferred in this project, Sloth implementation is mostly a result of basic testing needs and is not very sophisticated!

## Implementation

1. Install Sloth with Helm: `helm install sloth sloth/sloth`. There is no need to change the default configuration, but feel free to do so if You are interested in it.
2. Use the proposed [helm chart](../helm-charts/sloth-slos-traefik-services/) to implement availability and latency monitoring of services behind Traefik.
3. Refer to [Sloth documentation]() for additional SLO configuration.
4. If using Flux, there is an example [Flux usage](../flux/sloth/).
