# SLO Exporter

This tool makes it possible to ingest data either by querying Prometheus or by directly parsing it from service logs. SLO Exporter was tested for both situations and also for various SLO types (availability, latency, and quality). The configuration-heavy, but universal approach, makes the exporter an appropriate candidate for templating or setting some values by default. Thus, a helm chart was created, defaulting to the most common pipeline type and using templated modules to easily enforce the desired behavior (but only Traefik module was created in the testing process).

## Implementation

The implementation depends on the specific use case.

* Data from Prometheus: use the [slo-exporter-modules](../helm-charts/slo-exporter-modules/) helm chart. The chart has its own documentation.
* Data from Prometheus, custom pipeline: use [slo-exporter](../helm-charts/slo-exporter/) helm chart. SLO Exporter offered Kubernetes manifests but lacked their remake into a helm chart. I created one from their official manifests.
* Data from logs: get inspired by configured [server-log-generator](../slo-exporter/helm-charts/server-log-generator/values.yaml), where the exporter is configured through `sidecarContainers`, `extraVolumes`, and `extraManifests`. You might need to rewrite the log parser to suit Your service.
* Combination: You can use a single SLO Exporter instance for Prometheus-based data, but each log source requires its own exporter. This was the intention of the makers, and the approach is not considered wasteful. You could even run more instances to query Prometheus, each calculating different SLOs.

* If using Flux, there is an example [Flux usage](../flux/slo-exporter/).

### Notes

If You are querying Traefik metrics, check the latency buckets inside Traefik. I followed the latency bucket recommendations of Google (from [The Site Reliability Workbook](https://sre.google/workbook/alerting-on-slos/), especially `Alerting at Scale`) and had to change the bucket settings to reflect my desires:

```yaml
metrics:
  prometheus:
    buckets: "0.1, 0.2, 1.0, 5.0"
```

## Visualization

Data from SLO Exporter needs to be processed. I decided to do this in Prometheus [recording_rules.yml](../_values_override/prometheus.yaml?ref_type=heads#L10-38), because it creates a single source for both Grafana and Alermanager.

**`ratio_rate` should be an interval, that is useful in given context. Example values are for testing purposes only.**

Alerting is not part of this project, but would be calculated from ratio_rate. Multiwindow, Multi-Burn-Rate Alerts are recomended by Google - [Alerting](https://sre.google/workbook/alerting-on-slos/).

### Grafana Dashboards

* [Service Level Objectives (Class Overview)](../slo_class_overview.json)
* [Service Level Objectives (Service Detail)](../slo_service_detail.json)
