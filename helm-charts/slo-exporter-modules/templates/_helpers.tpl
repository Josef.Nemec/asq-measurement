{{/* No module */}}
{{- define "no_module queries" }}
  {{- if .queries }}{{- toYaml .queries }}{{- end }}
{{- end }}
{{- define "no_module eventKeyGenerator_keys" }}
  {{- if .eventKeyGenerator_keys }}{{- toYaml .eventKeyGenerator_keys }}{{- end }}
{{- end }}
{{- define "no_module rules" }}
  {{- if .rules }}{{- toYaml .rules }}{{- end }}
{{- end }}

{{/* Traefik module */}}
{{- define "traefik_module queries" }}
{{- if .availability }}
- query: "traefik_service_requests_total"
  type: counter_increase
  interval: 30s
  offset: 5m
  additionalLabels:
    event_type: http_request_result
{{- end }}
{{- if .latency }}
- query: "traefik_service_request_duration_seconds_bucket"
  type: histogram_increase
  interval: 30s
  additionalLabels:
    event_type: http_request_latency
{{- end }}
{{- end }}
{{- define "traefik_module eventKeyGenerator_keys" }}
- service
{{- end }}
{{- define "traefik_module rules" }}
{{- if .availability }}
- metadata_matcher:
    - key: event_type
      operator: isEqualTo
      value: http_request_result
  failure_conditions:
    - key: code
      operator: numberIsEqualOrHigherThan
      value: 500
  additional_metadata:
    slo_type: availability
{{- end }}
{{- if .latency }}
{{- range .latency_thresholds }}
- metadata_matcher:
    - key: event_type
      operator: isEqualTo
      value: http_request_latency
  {{- if .matcher }}
  slo_matcher:
    {{- toYaml .matcher | nindent 4 }}
  {{- end }}
  failure_conditions:
    - operator: numberIsHigherThan
      key: le
      value: {{ .latency99 }}
  additional_metadata:
    slo_type: latency99
- metadata_matcher:
    - key: event_type
      operator: isEqualTo
      value: http_request_latency
  {{- if .matcher }}
  slo_matcher:
    {{- toYaml .matcher | nindent 4 }}
  {{- end }}
  failure_conditions:
    - operator: numberIsHigherThan
      key: le
      value: {{ .latency90 }}
  additional_metadata:
    slo_type: latency90
{{- end }}
{{- end }}
{{- end }}
