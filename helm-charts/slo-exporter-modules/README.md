# SLO Exporter modules Chart

Helm chart for [seznam/slo-exporter](https://github.com/seznam/slo-exporter), using `prometheusIngester`, with custom modules for various services.

## Usage

Enable module by changing `.Values.config.src.[module].enabled` to `true`. The `values.yaml` file contains example usage.

For general SLO definitions use the `no_module` "module".

## Usage (module extensions)

If Your `no_module` specification becomes too long, You should turn somewhat general SLO definitions into modules. This is done in the `templates/_helpers.tpl`, no need to change anything in the actual templates. Feel free to use the `prometheus_module` as an example, it's been around since day one.

Simply:
1. Add new module template into `templates/_helpers.tpl`
2. Enable it and pass the necessary module values in `values.yaml`

## Modules

* `no_module`: Technically not a module. Enables custom SLO definitions
* `traefik_module`: Measures Availability and Latency of [Traefik](https://github.com/traefik/traefik) services
