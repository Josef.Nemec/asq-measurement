#!/usr/bin/env python3

'''
usage: server_log_generator.py [-h] [--config CONFIG] [--logs LOGS] [--sleep SLEEP] [--sleep-jitter SLEEP_JITTER]

options:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        Path to config file. (default: config/server_log_generator.yaml)
  --logs LOGS, -l LOGS  Path to log file. (default: logs/server_log_generator.log)
  --sleep SLEEP, -s SLEEP
                        Sleep between requests in ms. (default: 1000)
  --sleep-jitter SLEEP_JITTER, -j SLEEP_JITTER
                        Sleep jitter in ms. (default: 300)
'''

from argparse import ArgumentParser
from time import sleep, gmtime, strftime
import random
import yaml

def choose_log_content(config, keywords):
    ''' Choose information based on configured probabilities '''
    information = []
    for keyword in keywords:
        number = random.uniform(0.0, 100.0)
        cumulative_probability = 0
        for item in config[keyword]:
            cumulative_probability += item["probability"]
            if number <= cumulative_probability:
                information.append(item)
                break

    return information

def generate_log(config):
    ''' Generate log based on configured probabilities '''
    try:
        client, service, code = choose_log_content(config, ["clients", "services", "http_codes"])
        # https://en.wikipedia.org/wiki/Common_Log_Format
        return f'{client["ip"]} - - [{strftime("%d/%b/%Y:%H:%M:%S %z", gmtime())}] "GET {service["ip"]} HTTP/1.0" {code["code"]} {random.randint(2000, 5000)}'
    except ValueError:
        return ''

def test_probability(config, keywords):
    ''' Check probability sum for items in list '''
    for keyword in keywords:
        probability = 0
        for item in config[keyword]:
            probability = probability + item["probability"]
        if probability > 100:
            print(f"{keyword}: Probability can't be more than a 100 percent!")
            raise SystemExit

def parse_arguments():
    ''' Returns parsed command line arguments '''
    parser = ArgumentParser()

    parser.add_argument('--config', '-c', default="config/server_log_generator.yaml", type=str,
                        help='Path to config file. (default: %(default)s)')
    parser.add_argument('--logs', '-l', default="logs/server_log_generator.log", type=str,
                        help='Path to log file. (default: %(default)s)')
    parser.add_argument('--sleep', '-s', default=1000, type=int,
                        help='Sleep between requests in ms. (default: %(default)s)')
    parser.add_argument('--sleep-jitter', '-j', default=300, type=int,
                        help='Sleep jitter in ms. (default: %(default)s)')

    return parser.parse_args()

def main(args):
    ''' Setup and main loop '''
    if args.sleep_jitter > args.sleep:
        print("Please make jitter less than sleep time.")
        raise SystemExit

    with open(args.config, 'r', encoding="utf-8") as config_file:
        cfg = yaml.safe_load(config_file)
        test_probability(cfg, ["clients", "services", "http_codes"])

        print("Interrupt to stop.\n")
        try:
            while True:
                log = generate_log(cfg)
                print(log)
                with open(args.logs, "a", encoding="utf-8") as log_file:
                    log_file.write(f'{log}\n')

                jitter = random.randint(-args.sleep_jitter, args.sleep_jitter)
                sleep((args.sleep + jitter) /1000) # in ms
        except KeyboardInterrupt:
            print("\n\nStopped after interruption.")

if __name__ == "__main__":
    main(parse_arguments())
