# Server Log Generator

Simple Python bot, generating server logs for testing purposes with [Common Log Format](https://en.wikipedia.org/wiki/Common_Log_Format). It comes with a Dockerfile and a Helm chart.

## Usage

```
usage: server_log_generator.py [-h] [--config CONFIG] [--logs LOGS]
                               [--sleep SLEEP] [--sleep-jitter SLEEP_JITTER]

options:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        Path to config file. (default:
                        config/server_log_generator.yaml)
  --logs LOGS, -l LOGS  Path to log file. (default:
                        logs/server_log_generator.log)
  --sleep SLEEP, -s SLEEP
                        Sleep between requests in ms. (default: 1000)
  --sleep-jitter SLEEP_JITTER, -j SLEEP_JITTER
                        Sleep jitter in ms. (default: 300)
```

Logs are generated on the standard output and inside of a log file.

## Configuration

Through the configuration file, `clients`, `services`, `http_codes` and their probabilities can be specified. Refer to `config/server_log_generator.yaml` for an example.

### Docker

1. The image can be built from this folder by running: `sudo docker build -t server_log_generator:1.0 .`

2. The configuration needs to be mounted before the container is run: `sudo docker run -v $(pwd)/config:/app/config server_log_generator:1.0 [ARGS]`

The container accepts command line arguments just as the program would.

### Helm

To use the image in Minikube, it needs to be built inside its environment:

1. Switch to Minikube environment: `eval $(minikube docker-env)`
2. Build the image there: `docker build -t server_log_generator:1.0 .`

Refer to the [chart's values](helm-chart/server-log-generator/values.yaml) for configuration options.
