#!/usr/bin/env python3

'''
usage: quality_app.py [-h] [--fetch-time FETCH_TIME]
                      [--fetch-jitter FETCH_JITTER] [--timeout TIMEOUT]

options:
  -h, --help            show this help message and exit
  --fetch-time FETCH_TIME, -f FETCH_TIME
                        Fetch duration in ms. (default: 1000)
  --fetch-jitter FETCH_JITTER, -j FETCH_JITTER
                        Fetch duration jitter in ms. (default: 200)
  --timeout TIMEOUT, -t TIMEOUT
                        Abort fetch after in ms. (default: 1190)
'''

from argparse import ArgumentParser
from time import sleep
import random
from prometheus_client import start_http_server, Counter

def parse_arguments():
    ''' Returns parsed command line arguments '''
    parser = ArgumentParser()

    parser.add_argument('--fetch-time', '-f', default=1000, type=int,
                        help='Fetch duration in ms. (default: %(default)s)')
    parser.add_argument('--fetch-jitter', '-j', default=200, type=int,
                        help='Fetch duration jitter in ms. (default: %(default)s)')
    parser.add_argument('--timeout', '-t', default=1190, type=int,
                        help='Abort fetch after in ms. (default: %(default)s)')

    return parser.parse_args()

def main(args):
    ''' Setup and main loop '''
    if args.fetch_jitter > args.fetch_time:
        print("Please make jitter less than fetch time.")
        raise SystemExit
    if args.fetch_jitter + args.fetch_time < args.timeout:
        print("The quality will be 100 percent.")

    c = Counter('quality_app_events', 'Number of events total', ['service','event_type','quality'])
    c.labels('data-server','fetch','undegraded')
    c.labels('data-server','fetch','degraded')
    start_http_server(8000)

    print("Serving on: http://localhost:8000/\n\nInterrupt to stop...")
    try:
        while True:
            jitter = random.randint(-args.fetch_jitter, args.fetch_jitter)
            fetch_time = args.fetch_time + jitter

            if fetch_time <= args.timeout:
                sleep(fetch_time / 1000) # in ms
                c.labels('data-server','fetch','undegraded').inc()
            else:
                sleep(args.timeout / 1000)
                c.labels('data-server','fetch','degraded').inc()

    except KeyboardInterrupt:
        print("\n\nStopped after interruption.")

if __name__ == "__main__":
    main(parse_arguments())
