# Quality App

Simple Python program for testing purposes. It simulates data server overload.
If event times out, based on given configuration, event is considered degraded (without optional quality improvement from the oveloaded server).

Undegraded and degraded events are published as Prometheus metrics.
It comes with a Dockerfile and a Helm chart.

## Usage

```
usage: quality_app.py [-h] [--fetch-time FETCH_TIME]
                      [--fetch-jitter FETCH_JITTER] [--timeout TIMEOUT]

options:
  -h, --help            show this help message and exit
  --fetch-time FETCH_TIME, -f FETCH_TIME
                        Fetch duration in ms. (default: 1000)
  --fetch-jitter FETCH_JITTER, -j FETCH_JITTER
                        Fetch duration jitter in ms. (default: 200)
  --timeout TIMEOUT, -t TIMEOUT
                        Abort fetch after in ms. (default: 1190)
```

Metrics are accessible at [http://localhost:8000/](http://localhost:8000/).

### Docker

1. The image can be built from this folder by running: `sudo docker build -t quality_app:1.0 .`

The container accepts command line arguments just as the program would.

### Helm

To use the image in Minikube, it needs to be built inside its environment:

1. Switch to Minikube environment: `eval $(minikube docker-env)`
2. Build the image there: `docker build -t quality_app:1.0 .`

Refer to the [chart's values](helm-chart/quality-app/values.yaml) for configuration options.
