# HTTP Bot

Simple Python bot, generating GET requests to 3 apps with specified timeout.

## Usage

```
usage: http_bot.py [-h] [--app1 APP1] [--app2 APP2] [--app3 APP3]
                        [--sleep SLEEP] [--sleep-jitter SLEEP_JITTER]
                        [--config CONFIG]

options:
  -h, --help            show this help message and exit
  --app1 APP1           Probability of app1 request in percent. (default:
                        50.0)
  --app2 APP2           Probability of app2 request in percent. (default:
                        35.0)
  --app3 APP3           Probability of app3 request in percent. (default:
                        15.0)
  --sleep SLEEP, -s SLEEP
                        Sleep between requests in ms. (default: 1000)
  --sleep-jitter SLEEP_JITTER, -j SLEEP_JITTER
                        Sleep jitter in ms. (default: 300)
  --config CONFIG, -c CONFIG
                        Path to url config file. (default: http_bot.config)
```

Supply the app addresses through config file.
