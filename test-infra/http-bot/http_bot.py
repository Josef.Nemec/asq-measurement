#!/usr/bin/env python3

'''
usage: http_bot.py [-h] [--app1 APP1] [--app2 APP2] [--app3 APP3]
                         [--sleep SLEEP] [--sleep-jitter SLEEP_JITTER]
                         [--config CONFIG]
'''

from argparse import ArgumentParser
from time import sleep
import random
import requests

class HttpBot:
    ''' Sends get request based on configured parameters '''
    def __init__(self, arguments):
        self.app1 = arguments.app1
        self.app2 = arguments.app2
        self.app3 = arguments.app3

        tmp = []
        with open(arguments.config, encoding="utf-8") as f:
            for line in f:
                tmp.append(line.rstrip())
        self.app1_path = tmp[0]
        self.app2_path = tmp[1]
        self.app3_path = tmp[2]

    def get(self):
        ''' Send get request '''
        number = random.uniform(0.0, 100.0)

        if number <= self.app1:
            path = self.app1_path
        elif number <= (self.app1 + self.app2):
            path = self.app2_path
        elif number <= (self.app1 + self.app2 + self.app3):
            path = self.app3_path
        else:
            path = ""

        res = requests.get(path, timeout=30)
        print(f"{res.status_code} {path}, time: {res.elapsed.total_seconds()}")

def parse_arguments():
    ''' Returns parsed command line arguments '''
    parser = ArgumentParser()

    parser.add_argument('--app1', default=50.0, type=float,
                        help='Probability of app1 request in percent. (default: %(default)s)')
    parser.add_argument('--app2', default=35.0, type=float,
                        help='Probability of app2 request in percent. (default: %(default)s)')
    parser.add_argument('--app3', default=15.0, type=float,
                        help='Probability of app3 request in percent. (default: %(default)s)')
    parser.add_argument('--sleep', '-s', default=1000, type=int,
                        help='Sleep between requests in ms. (default: %(default)s)')
    parser.add_argument('--sleep-jitter', '-j', default=300, type=int,
                        help='Sleep jitter in ms. (default: %(default)s)')
    parser.add_argument('--config', '-c', default="http_bot.config", type=str,
                        help='Path to url config file. (default: %(default)s)')

    return parser.parse_args()

def main(args):
    ''' Setup and main loop '''
    if args.app1 + args.app2 + args.app3 > 100:
        print("Probability can't be more than a 100 percent!")
        raise SystemExit
    if args.sleep_jitter > args.sleep:
        print("Please make jitter less than sleep time.")
        raise SystemExit

    http_bot = HttpBot(args)

    print("Interrupt to stop.\n")
    try:
        while True:
            http_bot.get()
            jitter = random.randint(-args.sleep_jitter, args.sleep_jitter)
            sleep((args.sleep + jitter)/1000) # in ms
    except KeyboardInterrupt:
        print("\n\nStopped after interruption.")

if __name__ == "__main__":
    main(parse_arguments())
