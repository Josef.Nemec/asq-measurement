#!/bin/bash

# Usage with the default configuration file location config/helm_charts: ./destroy_infra.sh
# Usage with custom configuration file: ./destroy_infra.sh [CONFIG_FILE]

HELM_CHARTS=config/helm_charts # File with line in format: "deployment_name ..."

function uninstall_charts() {
    # Destroys infra by uninstalling helm charts
    local tmp
    while read chart; do
        tmp=( $chart )
        helm uninstall ${tmp[0]}
    done <$HELM_CHARTS
}

# Check argument existence
if [ ! -z "$1" ]
  then
    HELM_CHARTS="$1"
fi

uninstall_charts
echo ""
kubectl delete ingressroute.traefik.io --all
