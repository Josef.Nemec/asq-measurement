# Kubernetes Testing Infrastructure

This repository contains a Kubernetes infrastructure for testing purposes. It enables the simulation of unavailability and latency on backend web applications. It also comes with basic monitoring.

Components:

* [Traefik](https://github.com/traefik/traefik)
* [Prometheus](https://github.com/prometheus/prometheus)
* [Grafana](https://github.com/grafana/grafana)
* [Toxiproxy](https://github.com/Shopify/toxiproxy)
* [HTTP-HTTPS-Echo](https://github.com/mendhak/docker-http-https-echo)

## Usage

0. You can use [minikube](https://minikube.sigs.k8s.io/docs/start/) for local testing: `minikube start`
1. Fetch helm repositories: `./add_helm_repos.sh`
2. Build the infrastructure: `./build_infra.sh`
3. Do the tests
4. Destroy the infrastructure: `./destroy_infra.sh`
5. (Optionally) Stop minikube: `minikube stop`

The script from step 2 port-forwards the components to localhost. Teminating it will stop the port-forwarding but will keep the infrastructure running. If needed, You can port-forward again by running: `kubectl port-forward deployment.apps/traefik 8000 9090 3000 8474`.

With local testing, the components are accessible at:

* Web-app1: [http://localhost:8000/app1](http://localhost:8000/app1)
* Web-app2: [http://localhost:8000/app2](http://localhost:8000/app2)
* Web-app3: [http://localhost:8000/app3](http://localhost:8000/app3)
* Prometheus: [http://localhost:9090](http://localhost:9090)
* Grafana: [http://localhost:3000](http://localhost:3000)
* [Toxiproxy API](https://github.com/Shopify/toxiproxy#endpoints): localhost:8474

Grafana username is `admin` and password is on the output of build_infra.sh or gained by running: `kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo`.

## Configuration

Configuration is done via configuration files.

You can customize the helm charts in the `values_override` folder. The script will use a file with the name `DEPLOYMENT_NAME.yaml`.

Helm charts and repositories can be added or removed under the `config` folder. Charts are in the line format: `deployment_name chart_name` and repositories in: `repo_name repo_url`. You can also use custom files and/or folders by specifying them using command line arguments: `./build_infra.sh --help`.

Traefik routes specification is also in the `config` folder.

To change the settings of Toxiproxy, use the `config/toxiproxy` folder. Proxies are specified in `proxies.json`, toxics to be applied in `toxics`, line format is: `proxy_name toxic_file(without .json)`. You can also use a custom toxiproxy folder.

If You need to save some charts locally, You can use the `charts` folder.

## Logs

To keep the `build_infra.sh` script output pretty, logs are redirected to files in the `logs` folder.

You might want to check the applied Toxiproxy configuration: `curl -s localhost:8474/proxies | jq`.
