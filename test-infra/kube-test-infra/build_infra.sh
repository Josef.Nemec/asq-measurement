#!/bin/bash

: '
Script building basic testing infrastructure in a Kubernetes cluster.
usage: ./build_infra.sh [-h | --help]
                        [-c | --helm-charts CHARTS_FILE]
                        [-o | --values-override OVERRIDE_FOLDER]
                        [-t | --toxiproxy-config TOXIPROXY_CONFIG_FOLDER]
                        [-p | --ports-forward PORTS]
                        [-w | --wait-for-pods WAIT_TIMEOUT]
'

HELM_CHARTS=config/helm_charts # File with line in format: "deployment_name chart_name"
VALUES_OVERRIDE=values_override # Folder containing custom helm values
TOXIPROXY_CONFIG=config/toxiproxy # Toxiproxy config folder
WAIT_TIMEOUT=120s

# 8000:web-apps, 9090:prometheus, 3000:grafana, 8474:toxiproxy-api
PORTS_FORWARD="8000 9090 3000 8474"

function setup_toxiproxy() {
    # Send toxiproxy configs (proxies and toxics) to API
    local ip_app1=$(kubectl get service/echo-server1 -o jsonpath='{.spec.clusterIP}')
    local ip_app2=$(kubectl get service/echo-server2 -o jsonpath='{.spec.clusterIP}')
    local ip_app3=$(kubectl get service/echo-server3 -o jsonpath='{.spec.clusterIP}')
    local tmp

    # Create proxies (populate)
    eval "curl -s -X POST -d '$(cat $TOXIPROXY_CONFIG/proxies.json)' localhost:8474/populate \
        | jq > logs/toxiproxy_populate.log"

    # Setup toxics
    date > logs/toxiproxy_toxics.log # Delete old logs
    while read proxy_toxic; do
        tmp=( $proxy_toxic )
        curl -s -X POST -d "$(cat ${TOXIPROXY_CONFIG}/${tmp[1]}.json)" \
            localhost:8474/proxies/${tmp[0]}/toxics \
            | jq >> logs/toxiproxy_toxics.log
    done <"${TOXIPROXY_CONFIG}/toxics"
}

function install_charts() {
    # Installs helm charts from config folder
    local tmp
    local values_file
    local log_file="logs/helm_install.log"
    date > $log_file # Delete old logs

    while read chart; do
        tmp=( $chart )
        values_file="${VALUES_OVERRIDE}/${tmp[0]}.yaml"
        if [ -e $values_file ]; then
            helm install -f $values_file ${tmp[0]} ${tmp[1]} >> $log_file
            echo "Installed ${tmp[0]}, config file: $values_file"
        else 
            helm install ${tmp[0]} ${tmp[1]} >> $log_file
            echo "Installed ${tmp[0]}, config file: None"
        fi
        echo -e "\n--------------------\n" >> $log_file
    done <$HELM_CHARTS
}

function show_help() {
    echo -e "
Script building basic testing infrastructure in a Kubernetes cluster.

usage: ./build_infra.sh [-h | --help]
                        [-c | --helm-charts CHARTS_FILE]
                        [-o | --values-override OVERRIDE_FOLDER]
                        [-t | --toxiproxy-config TOXIPROXY_CONFIG_FOLDER]
                        [-p | --ports-forward PORTS]
                        [-w | --wait-for-pods WAIT_TIMEOUT]
"
}

function parse_arguments() {
    # Parses allowed command line arguments
    local short=c:,o:,t:,p:,w:,h
    local long=helm-charts:,values-override:,toxiproxy-config:,ports-forward:,wait-for-pods:,help
    local opts=$(getopt --alternative --name "build_infra.sh" --options $short --longoptions $long -- "$@")

    echo -e "Working with arguments: $opts\n" # Debug
    eval set -- "$opts"

    while true; do
        case "$1" in
            -c | --helm-charts )
                HELM_CHARTS="$2"
                shift 2
                ;;
            -o | --values-override )
                VALUES_OVERRIDE="$2"
                shift 2
                ;;
            -t | --toxiproxy-config )
                TOXIPROXY_CONFIG="$2"
                shift 2
                ;;
            -p | --ports-forward )
                PORTS_FORWARD="$2"
                shift 2
                ;;
            -w | --wait-for-pods )
                WAIT_TIMEOUT="$2"
                shift 2
                ;;
            -h | --help)
                show_help
                exit 2
                ;;
            --)
                shift;
                break
                ;;
            *)
                echo "Unexpected option: $1"
                ;;
        esac
    done
}

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT # Kill background processes on termination
set -e

if [ $# -ne 0 ]
  then
    parse_arguments "$@"
fi

install_charts
echo -e "\nWaiting max $WAIT_TIMEOUT for pods to start..."
kubectl wait --for=condition=Ready pod --all --timeout=$WAIT_TIMEOUT
echo ""

kubectl apply -f ./config/traefik_IngressRoute.yaml
kubectl port-forward deployment.apps/traefik $PORTS_FORWARD > logs/traefik_portforward.log &
sleep 5s
echo -e "\nGrafana admin user password: $(kubectl get secret --namespace default grafana \
    -o jsonpath="{.data.admin-password}" | base64 --decode ; echo)"
echo "Forwarding ports $PORTS_FORWARD"

setup_toxiproxy

#echo "Build finished successfully."
