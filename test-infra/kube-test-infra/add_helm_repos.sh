#!/bin/bash

# Usage with the default configuration file location config/helm_repos: ./add_helm_repos.sh
# Usage with custom configuration file: ./add_helm_repos.sh [CONFIG_FILE]

HELM_REPOS=config/helm_repos # File with line in format: "repo_name repo_url"

function add_repos() {
    # Adds helm repositories from config file
    local tmp
    while read repo; do
        tmp=( $repo )
        helm repo add ${tmp[0]} ${tmp[1]}
    done <$HELM_REPOS
    echo ""
    helm repo update
}

# Check argument existence
if [ ! -z "$1" ]
  then
    HELM_REPOS="$1"
fi

add_repos
